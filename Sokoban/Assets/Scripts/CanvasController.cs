using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CanvasController : MonoBehaviour
{
    public Text textoNivel;
    public Text textoMovimientos;
    public static int numNivel;

    // Start is called before the first frame update
    void Start()
    {

        if (PlayerPrefs.GetInt("iniciarNum") == 1)
        {
            numNivel = 1;
            PlayerPrefs.SetInt("iniciarNum",2);
        }
    }

    // Update is called once per frame
    void Update()
    {
        textoMovimientos.text = MovimientoJugador.movimientos.ToString();
    }

    public void SubirNivel()
    {
        if(numNivel <= 9)
        {
            numNivel++;
            textoNivel.text = numNivel.ToString();
        }
    }    
    public void BajarNivel()
    {
        if(numNivel >= 2)
        {
            numNivel--;
            textoNivel.text = numNivel.ToString();
        }
    }

    public void actualizarNivel()
    {
        textoNivel.text = numNivel.ToString();
    }

    public void irMenu()
    {
        SceneManager.LoadScene(0);
    }
}
