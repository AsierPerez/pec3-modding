using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetInt("iniciarNum", 1);
    }

    public void IrJuego()
    {
        PlayerPrefs.SetInt("esJuego", 1);
        SceneManager.LoadScene(2);
    }

    public void IrCreador()
    {
        PlayerPrefs.SetInt("esJuego", 2);
        SceneManager.LoadScene(1);
    }

    public void Salir()
    {
        Application.Quit();
    }
}
