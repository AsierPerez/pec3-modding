using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Empuje : MonoBehaviour
{
    private GameObject[] Obstaculos;
    private GameObject[] Cajas;
    private GameObject[] Metas;
    private int cantidadMetas;
    // Start is called before the first frame update
    void Start()
    {
        Obstaculos = GameObject.FindGameObjectsWithTag("Obstaculos");
        Cajas = GameObject.FindGameObjectsWithTag("Cajas");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool Mover(Vector2 direccion)
    {
        if (BloqueoMover(transform.position, direccion))
        {
            return false;
        }
        else
        {
            transform.Translate(direccion);
            return true;
        }
    }

    public bool BloqueoMover(Vector3 posicion, Vector2 direccion)
    {
        Vector2 nuevaPos = new Vector2(posicion.x, posicion.y) + direccion;

        foreach (var obj in Obstaculos)
        {
            if (obj.transform.position.x == nuevaPos.x && obj.transform.position.y == nuevaPos.y)
            {
                return true;
            }
        }

        foreach (var obj in Cajas)
        {
            if (obj.transform.position.x == nuevaPos.x && obj.transform.position.y == nuevaPos.y)
            {
                return true;
            }
        }
        return false;
    }
}
