using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class GuardarCargarJson : MonoBehaviour
{
    private GameObject[] Posiciones;
    private List<GameObject> queObjetoEs;
    private List<GameObject> posObjeto;

    public GameObject pared;
    public GameObject caja;
    public GameObject meta;
    public GameObject jugador;


    // Start is called before the first frame update
    void Start()
    {
        Posiciones = GameObject.FindGameObjectsWithTag("Posicion");
        queObjetoEs = new List<GameObject>();
        posObjeto = new List<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GuardarJson()
    {
        string newJsonString = "";
        int i = 1;
        List<soyBloque> listOfChannels = new List<soyBloque>();
        ColleccionBloques col = new ColleccionBloques();
        foreach (var obj in Posiciones)
        {
            soyBloque bloque = new soyBloque();
            if (obj.GetComponent<ElegirBloque>().soyVacio)
            {
                bloque.queSoy = "vacio";
                bloque.quienSoy = i;
            }
            else if (obj.GetComponent<ElegirBloque>().soyPared)
            {
                bloque.queSoy = "pared";
                bloque.quienSoy = i;
            }
            else if (obj.GetComponent<ElegirBloque>().soyCaja)
            {
                bloque.queSoy = "caja";
                bloque.quienSoy = i;
            }
            else if (obj.GetComponent<ElegirBloque>().soyMeta)
            {
                bloque.queSoy = "meta";
                bloque.quienSoy = i;
            }
            else if (obj.GetComponent<ElegirBloque>().soyJugador)
            {
                bloque.queSoy = "jugador";
                bloque.quienSoy = i;
            }

            listOfChannels.Add(new soyBloque { queSoy = bloque.queSoy, quienSoy = bloque.quienSoy});

            col.bloques = listOfChannels.ToArray();

            newJsonString = JsonUtility.ToJson(col);
            Debug.Log(newJsonString);
            File.WriteAllText(Application.dataPath + "/Resources/Nivel" + CanvasController.numNivel + ".json", newJsonString);

            i++;
        }

    }

    public void LeerJson(GameObject[] Posiciones)
    {
        string json = File.ReadAllText(Application.dataPath + "/Resources/Nivel" + CanvasController.numNivel + ".json");

        ColleccionBloques bloques = JsonUtility.FromJson<ColleccionBloques>(json);

        List<soyBloque> bloque = new List<soyBloque>();
        int i = 0;
        foreach (var obj in Posiciones)
        {
            bloque.Add(bloques.bloques[i]);
            string queEs = bloque[i].queSoy;
            obj.GetComponent<ElegirBloque>().CrearObjetos(queEs);

            i++;
        }
    }

    public void editarNivel()
    {
        string json = File.ReadAllText(Application.dataPath + "/Resources/Nivel" + CanvasController.numNivel + ".json");
        ColleccionBloques bloques = JsonUtility.FromJson<ColleccionBloques>(json);

        List<soyBloque> bloque = new List<soyBloque>();
        int i = 0;
        foreach (var obj in Posiciones)
        {
            bloque.Add(bloques.bloques[i]);
            string queEs = bloque[i].queSoy;
            obj.GetComponent<ElegirBloque>().CrearObjEditar(queEs);

            i++;
        }
    }

    public void Limpiar()
    {
        foreach (var obj in Posiciones)
        {
            obj.GetComponent<ElegirBloque>().Limpiar();
        }
    }

    public void LimpiarEditor()
    {
        foreach (var obj in Posiciones)
        {
            obj.GetComponent<ElegirBloque>().LimpiarEditor();
        }
    }
}
