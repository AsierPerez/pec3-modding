using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoJugador : MonoBehaviour
{

    private GameObject[] Obstaculos;
    private GameObject[] Cajas;

    public static int movimientos;

    private bool movimientoListo;
    // Start is called before the first frame update
    void Start()
    {
        movimientos = 0;
        Obstaculos = GameObject.FindGameObjectsWithTag("Obstaculos");
        Cajas = GameObject.FindGameObjectsWithTag("Cajas");
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 moverse = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        moverse.Normalize();

        if(moverse.sqrMagnitude > 0.5)
        {
            if (movimientoListo)
            {
                movimientoListo = false;
                Mover(moverse);
                movimientos++;
            }
        }
        else
        {
            movimientoListo = true;
        }

    }

    public bool Mover(Vector2 direccion)
    {
        if(Mathf.Abs(direccion.x) < 0.5)
        {
            direccion.x = 0;
        }
        else
        {
            direccion.y = 0;
        }
        direccion.Normalize();

        if(Bloqueo(transform.position, direccion))
        {
            return false;
        }
        else
        {
            transform.Translate(direccion);
            return true;
        }
    }

    public bool Bloqueo(Vector3 posicion, Vector2 direccion)
    {
        Vector2 nuevaPos = new Vector2(posicion.x, posicion.y) + direccion;

        foreach(var obj in Obstaculos)
        {
            if(obj.transform.position.x == nuevaPos.x && obj.transform.position.y == nuevaPos.y)
            {
                return true;
            }
        }

        foreach(var obj in Cajas)
        {
            if(obj.transform.position.x == nuevaPos.x && obj.transform.position.y == nuevaPos.y)
            {
                Empuje objEmpuje = obj.GetComponent<Empuje>();
                if(objEmpuje && objEmpuje.Mover(direccion))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        return false;
    }
}
