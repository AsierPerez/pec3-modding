using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meta : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Cajas")
        {
            collision.GetComponent<SpriteRenderer>().color = Color.green;
            GameController.metasConseguidas++;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {

        if (collision.tag == "Cajas")
        {
            collision.GetComponent<SpriteRenderer>().color = Color.white;
            GameController.metasConseguidas--;
        }
    }
}
