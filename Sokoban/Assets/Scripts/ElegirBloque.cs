using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElegirBloque : MonoBehaviour
{
    public int queBloque;
    public Sprite vacio;
    public Sprite pared;
    public Sprite caja;
    public Sprite meta;
    public Sprite jugador;

    public bool soyVacio;
    public bool soyPared;
    public bool soyCaja;
    public bool soyMeta;
    public bool soyJugador;

    public GameObject paredObj;
    public GameObject cajaObj;
    public GameObject metaObj;
    public GameObject jugadorObj;

    public static bool jugadorColocado;
    // Start is called before the first frame update
    void Start()
    {
        queBloque = 1;
        soyVacio = true;
        soyPared = false;
        soyCaja = false;
        soyMeta = false;
        soyJugador = false;
        jugadorColocado = false;
    }

    private void OnMouseDown()
    {
        if(PlayerPrefs.GetInt("esJuego") == 2)
        {
            if (queBloque == 0 && soyJugador)
            {
                jugadorColocado = false;
                soyJugador = false;
            }

            if (queBloque == 0)
            {
                this.GetComponent<SpriteRenderer>().color = new Color(54, 58, 66);
                this.GetComponent<SpriteRenderer>().sprite = vacio;
                soyVacio = true;
                soyPared = false;
                soyCaja = false;
                soyMeta = false;
                queBloque = 1;
            }
            else if (queBloque == 1)
            {
                this.GetComponent<SpriteRenderer>().color = Color.white;
                this.GetComponent<SpriteRenderer>().sprite = pared;
                soyVacio = false;
                soyPared = true;
                soyCaja = false;
                soyMeta = false;
                queBloque = 2;
            }
            else if (queBloque == 2)
            {
                this.GetComponent<SpriteRenderer>().sprite = caja;
                soyVacio = false;
                soyPared = false;
                soyCaja = true;
                soyMeta = false;
                queBloque = 3;
            }
            else if (queBloque == 3)
            {
                this.GetComponent<SpriteRenderer>().sprite = meta;
                soyVacio = false;
                soyPared = false;
                soyCaja = false;
                soyMeta = true;

                if (!jugadorColocado)
                    queBloque = 4;
                else
                    queBloque = 0;
            }
            else if (queBloque == 4)
            {
                this.GetComponent<SpriteRenderer>().sprite = jugador;
                jugadorColocado = true;
                soyVacio = false;
                soyPared = false;
                soyCaja = false;
                soyMeta = false;
                soyJugador = true;
                queBloque = 0;
            }
        }
    }

    public void CrearObjetos(string queEs)
    {
        GameObject objeto = new GameObject();
        if (queEs.Equals("pared"))
        {
            objeto = Instantiate(paredObj, this.transform);
        }
        else if (queEs.Equals("caja"))
        {
             objeto = Instantiate(cajaObj, this.transform);
        }
        else if (queEs.Equals("meta"))
        {
             objeto = Instantiate(metaObj, this.transform);
        }
        else if (queEs.Equals("jugador"))
        {
             objeto = Instantiate(jugadorObj, this.transform);
        }

        objeto.transform.parent = gameObject.transform;
    }


    public void CrearObjEditar(string queEs)
    {
        if (queEs.Equals("pared"))
        {
            this.GetComponent<SpriteRenderer>().color = Color.white;
            this.GetComponent<SpriteRenderer>().sprite = pared;
            soyVacio = false;
            soyPared = true;
            soyCaja = false;
            soyMeta = false;
            queBloque = 2;
        }
        else if (queEs.Equals("caja"))
        {
            this.GetComponent<SpriteRenderer>().color = Color.white;
            this.GetComponent<SpriteRenderer>().sprite = caja;
            soyVacio = false;
            soyPared = false;
            soyCaja = true;
            soyMeta = false;
            queBloque = 3;
        }
        else if (queEs.Equals("meta"))
        {
            this.GetComponent<SpriteRenderer>().color = Color.white;
            this.GetComponent<SpriteRenderer>().sprite = meta;
            soyVacio = false;
            soyPared = false;
            soyCaja = false;
            soyMeta = true;

            if (!jugadorColocado)
                queBloque = 4;
            else
                queBloque = 0;
        }
        else if (queEs.Equals("jugador"))
        {
            this.GetComponent<SpriteRenderer>().color = Color.white;
            this.GetComponent<SpriteRenderer>().sprite = jugador;
            jugadorColocado = true;
            soyVacio = false;
            soyPared = false;
            soyCaja = false;
            soyMeta = false;
            soyJugador = true;
            queBloque = 0;
        }
        else if (queEs.Equals("vacio"))
        {
            this.GetComponent<SpriteRenderer>().color = new Color(54, 58, 66);
            this.GetComponent<SpriteRenderer>().sprite = vacio;
            soyVacio = true;
            soyPared = false;
            soyCaja = false;
            soyMeta = false;
            queBloque = 1;
        }
    }

    public void Limpiar()
    {
        Destroy(this.transform.GetChild(0).gameObject);
    }

    public void LimpiarEditor()
    {
        this.GetComponent<SpriteRenderer>().color = new Color(54, 58, 66);
        this.GetComponent<SpriteRenderer>().sprite = vacio;
        soyVacio = true;
        soyPared = false;
        soyCaja = false;
        soyMeta = false;
        soyJugador = false;
        jugadorColocado = false;
        queBloque = 1;
    }
}
