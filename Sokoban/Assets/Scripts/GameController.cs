using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{

    private GameObject[] Metas;
    private GameObject[] Posiciones;
    private int cantidadMetas;

    public CanvasController canvasControl;
    public GameObject sigNiv;
    public GameObject victoriaFin;
    private bool unaVez;
    private bool fin;

    public static int metasConseguidas;

    public bool empiezaJuego;

    public AudioClip sig;
    public AudioClip victoriaS;
    public AudioSource audioS;

    // Start is called before the first frame update
    void Start()
    {

        Posiciones = GameObject.FindGameObjectsWithTag("Posicion");

        fin = false;
        sigNiv.SetActive(false);
        victoriaFin.SetActive(false);
        unaVez = true;
        if (empiezaJuego)
        {
            this.gameObject.GetComponent<GuardarCargarJson>().LeerJson(Posiciones);
        }

        Metas = GameObject.FindGameObjectsWithTag("Meta");
        metasConseguidas = 0;
        foreach (var obj in Metas)
        {
            cantidadMetas++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (cantidadMetas == metasConseguidas && unaVez)
        {
            if(CanvasController.numNivel <= 9)
            {
                StartCoroutine(pasarNivel());
            }
            else if(CanvasController.numNivel == 10)
            {
                StartCoroutine(victoria());
            }
            unaVez = false;
        }


        if (fin)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene(0);
            }
        }
    }

    public void siguenteNivel()
    {
        SceneManager.LoadScene(2);
    }

    public void Reiniciar()
    {
        SceneManager.LoadScene(2);
    }

    IEnumerator pasarNivel()
    {
        yield return new WaitForSeconds(1f);
        sigNiv.SetActive(true);
        CanvasController.numNivel++;
        canvasControl.actualizarNivel();
        audioS.PlayOneShot(sig);
        yield return new WaitForSeconds(3f);
        siguenteNivel();
    }

    IEnumerator victoria()
    {
        yield return new WaitForSeconds(1f);
        audioS.PlayOneShot(victoriaS);
        victoriaFin.SetActive(true);
        fin = true;
    }
}
